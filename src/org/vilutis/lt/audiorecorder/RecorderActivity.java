
/*
 * The application needs to have the permission to write to external storage
 * if the output file is written to the external storage, and also the
 * permission to record audio. These permissions must be set in the
 * application's AndroidManifest.xml file, with something like:
 *
 * <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 * <uses-permission android:name="android.permission.RECORD_AUDIO" ></uses-permission>
 */

package org.vilutis.lt.audiorecorder;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Checkable;

public class RecorderActivity extends Activity {

	private static final String LOG_TAG = "RecorderActivity";
	
	private static String mFileName = null;

	private MediaRecorder mRecorder = null;

	private MediaPlayer mPlayer = null;
	
	private boolean isRecording = false;

	private Button playButton;

	private Button recordButton;
	
	private Button saveFile;

	private Checkable loop;

	protected final BitmapDrawable playIcon;
	
	private void startPlaying() {
		if( isRecording ) {
			stopRecording();
		}
		
		mPlayer = new MediaPlayer();
		mPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				playButton.setText(R.string.start_playing);
				playButton.setCompoundDrawables(playIcon, null, null, null);
			}
		});
		try {
			mPlayer.setDataSource(mFileName);
			mPlayer.setLooping(loop.isChecked());
			mPlayer.prepare();
			mPlayer.start();
			playButton.setText(R.string.stop_playing);
		} catch (IOException e) {
			Log.e(LOG_TAG, "prepare() failed");
		}

	}

	private void stopPlaying() {
		if ( isPlaying() ) {
			mPlayer.release();
			mPlayer = null;
		}
		playButton.setText(R.string.start_playing);
	}

	private boolean isPlaying() {
		return mPlayer != null && mPlayer.isPlaying();
	}

	private void startRecording() {
		if ( isPlaying() ) {
			stopPlaying();
		}
		
		mRecorder = new MediaRecorder();
		
		try {
			mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		} catch (RuntimeException e) {
			Log.e(LOG_TAG, "setAudioSource() failed");
			return;
		}
		
		mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		mRecorder.setOutputFile(mFileName);
		mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		
		try {
			mRecorder.prepare();
		} catch (IOException e) {
			Log.e(LOG_TAG, "prepare() failed");
		}

		mRecorder.start();
		isRecording = true;
		recordButton.setText(R.string.stop_recording);
	}

	private void stopRecording() {
		isRecording = false;
		mRecorder.stop();
		mRecorder.release();
		mRecorder = null;
		recordButton.setText(R.string.start_recording);
	}

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		
		setContentView(R.layout.activity_recorder);
		
		this.recordButton = (Button) findViewById(R.id.btn_record);
		
		initRecordButton(recordButton);
		
		this.playButton = (Button) findViewById( R.id.btn_play );
		
		this.loop = (Checkable) findViewById( R.id.chk_loop );
		this.loop.setChecked( true );

		initPlayButton( playButton, loop );
		
		this.saveFile = (Button) findViewById( R.id.btn_save );
		
		initSaveButton( saveFile );
		
	}

	private void initSaveButton(final Button saveFile) {
		saveFile.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});
	}

	private void initRecordButton(final Button recordButton) {
		recordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isRecording) {
					stopRecording();
				} else {
					startRecording();
				}
			}
		});
	}

	private void initPlayButton(final Button playButton, final Checkable loop) {
		playButton.setOnClickListener( new OnClickListener() {
			
			public void onClick(View v) {
				if (isPlaying()) {
					stopPlaying();
				} else {
					startPlaying();
				}
			}
		} );
		
		playButton.setEnabled(new File(mFileName).exists());
	}
	



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.recorder, menu);
		return true;
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mRecorder != null) {
			mRecorder.release();
			mRecorder = null;
		}

		if (mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}

	public RecorderActivity() {
		mFileName = Environment.getExternalStorageDirectory().getAbsolutePath();
		mFileName += "/audiorecordtest.3gp";
		Resources resources = Resources.getSystem();
		playIcon = new BitmapDrawable( resources, BitmapFactory.decodeStream(this.getClass().getResourceAsStream("playIcon.png")));
	}

}
